'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); // Copyright Notice: All rights reserved. © 2016 - 2017 Marvin Danig
// FLIPPY VERSION::0.0.1'

var _mode = require('../modules/mode.js');

var _mode2 = _interopRequireDefault(_mode);

require('../modules/graph.js');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// import events from '../modules/events.js'

(function (w, d, undefined) {
    /***********************************
     ************* Public API **********
     ***********************************/

    var Book = function () {
        function Book() {
            _classCallCheck(this, Book);

            this.mode = _mode2.default.getMatch('(orientation: landscape)') ? 'landscape' : 'portrait';
        }

        _createClass(Book, [{
            key: 'getLength',
            value: function getLength() {
                return _book.pages.length;
            }
        }, {
            key: 'view',
            value: function view() {
                return _book.currentView;
            }
        }, {
            key: 'flip',
            value: function flip(page_no) {
                _book.currentPage = page_no;
                console.log(_book.currentPage);
                //TODO: Calculate pages and print the book
            }
        }]);

        return Book;
    }();

    /***********************************
     ********** Private Methods ********
     ***********************************/

    var _book = new Book();

    function _init(node) {
        var settings = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : { duration: 500, animation: true, curl: true, peel: true, zoom: true };


        _book.node = node;

        _book.settings = settings;

        var nodes = [].concat(_toConsumableArray(node.children));

        _book.buttons = nodes.splice(0, 2);

        _book.pages = nodes.map(function (page, index) {
            return _addBaseClasses(page, index);
        });

        _removeChildren(node);

        _book.currentPage = settings.start_page === undefined ? 1 : parseInt(settings.start_page) > 0 ? parseInt(settings.start_page) % parseInt(_book.pages.length) : parseInt(settings.start_page) % parseInt(_book.pages.length) !== 0 ? parseInt(_book.pages.length) + 1 + parseInt(settings.start_page) % parseInt(_book.pages.length) : 1;

        _setView(_book.currentPage);

        _setRange(_book.currentPage);

        // _printBook()

        return;
    }

    _mode2.default.onChange('(orientation: landscape)', function (match) {
        _book.mode = match ? 'landscape' : 'portrait';

        // _setViewAndRange(_book.currentPage)

        // _printBook()
        // _applyMode(_book.node)
    });

    function _removeChildren(node) {
        node.innerHTML = '';
        // Which is performant?
        // while (node.hasChildNodes())         
        // node.removeChild(node.lastChild)
    }

    function _addBaseClasses(pageObj, index) {
        pageObj.classList.add('page-' + (parseInt(index) + 1));
        if (isEven(index)) pageObj.classList.add('page-even');else pageObj.classList.add('page-odd');

        return pageObj;

        // let wrappedHtml = wrapHtml(pageObj)
        // return wrappedHtml
    }

    function _wrapHtml(pageObj) {
        var newWrapper = document.createElement('div'); // May be required. May not be required.

        newWrapper.setAttribute('style', 'pointer-events: none; position:absolute; top: 0; left :0; overflow:none; height: 100%;');
        newWrapper.classList.add('page');
        newWrapper.appendChild(pageObj);

        return newWrapper;
    }

    function _setView() {
        var currentPage = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;

        switch (_book.mode) {
            case 'portrait':
                _book.currentView = [parseInt(currentPage)];
                break;

            case 'landscape':
                if (isEven(parseInt(currentPage))) {
                    _book.currentView = [parseInt(currentPage), parseInt(currentPage + 1)];
                }
                if (isOdd(parseInt(currentPage))) {
                    _book.currentView = [parseInt(currentPage - 1), parseInt(currentPage)];
                }
                break;
        }

        return;
    }

    function _setRange() {
        var currentPage = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;


        /*** 
            @range = _book.pages.slice(P , Q) where:
                P & Q are integers 
                P & Q may or may not lie in the range 0 < VALUES < 2N (_book.length) 
        ***/

        switch (_book.mode) {
            case 'portrait':
                _book.range = _book.pages.slice(parseInt(_book.currentView[0]) - 3, parseInt(_book.currentView[0]) + 3);
                break;

            case 'landscape':
                _book.range = _book.pages.slice(parseInt(_book.currentView[0]) - 3, parseInt(_book.currentView[1]) + 2);

                break;
        }

        return;
    }

    /***********************************
     *********** DOM Printing **********
     ***********************************/

    function _printBook() {

        _printButtons(_book.node);

        // _printView(_book.node)


        // console.log('CURRENT PAGE', _book.currentPage)

        // console.log('VIEW', _book.currentView)

        // console.log('RANGE', _book.range)

        // console.log('BOOK', _book.pages)        

        return;
    }

    function _printView(node) {

        var docfrag = document.createDocumentFragment();

        // for (let i = 0; i < 2; i++) {
        //     docfrag.appendChild(_book.buttons[i]) // Add button objects to frag.
        // }

        // const pageList = _book.range.map((page, index) => {
        //     return _applyStyles(page, index)
        // })

        _book.range.forEach(function (page) {
            docfrag.appendChild(page);
        });

        var newElem = node.cloneNode();
        newElem.innerHTML = '';
        newElem.appendChild(docfrag);

        node.parentNode.replaceChild(newElem, node);
        // node.appendChild(docfrag) 
        return;
    }

    function _printButtons(node) {
        for (var i = 0; i < 2; i++) {
            node.appendChild(_book.buttons[i]);
        }
    }

    function _applyMode() {
        switch (_book.mode) {
            case 'portrait':
                break;
            case 'landscape':
                break;
        }
    }

    function _applyStyles(pageObj, index) {
        switch (_book.mode) {
            case 'portrait':
                if (pageObj.classList.contains('left')) pageObj.classList.remove('left'); // Inline style?
                if (pageObj.classList.contains('right')) pageObj.classList.remove('right');

                break;
            case 'landscape':
                var classes = isEven(index) ? 'left' : 'right';
                if (!pageObj.classList.contains(classes)) pageObj.classList.add(classes);
                break;
        }
        pageObj.setAttribute('style', 'background: rgba(0, 100, 200, 0.4);');
        return pageObj;
    }

    /**********************************/
    /********* Helper methods *********/
    /**********************************/

    function isEven(n) {
        return n === parseFloat(n) ? !(n % 2) : void 0;
    }

    function isOdd(n) {
        return Math.abs(n % 2) == 1;
    }

    /**********************************/
    /*********** Exposed API **********/
    /**********************************/

    var Superbook = function () {
        function Superbook() {
            _classCallCheck(this, Superbook);
        }

        _createClass(Superbook, [{
            key: 'flippy',
            value: function flippy(methodName) {
                for (var _len = arguments.length, theArgs = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
                    theArgs[_key - 1] = arguments[_key];
                }

                switch (methodName) {
                    case 'length':
                        return _book['getLength'].apply(_book, theArgs);
                        break;
                    default:
                        return _book[methodName].apply(_book, theArgs);
                        break;
                }
            }
        }]);

        return Superbook;
    }();

    // Putting superbook object in global namespace.


    if (typeof w.Flippy === 'undefined') {
        w.Flippy = {
            init: function init(node, settings) {
                _init(node, settings);
                return new Superbook();
            }
        };
    }
})(window, document);